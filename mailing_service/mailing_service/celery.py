import os
from celery import Celery


os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'mailing_service.settings')
app = Celery('mailing_service', namespace='CELERY')
app.config_from_object('django.conf:settings')

app.autodiscover_tasks()
