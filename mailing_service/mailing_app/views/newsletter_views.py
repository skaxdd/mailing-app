from django.db.models import Count, Q
from drf_spectacular.utils import extend_schema
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from ..models import Newsletter
from ..utils.response_scheme import (
    newsletter_messages_response_200,
    newsletter_message_stats_response_200
)
from ..serializers import (
    NewsletterListSerializer,
    NewsletterPostSerializer,
    NewsletterRetrieveSerializer,
    MessageSerializer
)
from ..enums import MessageStatus


class NewsletterViewSet(viewsets.ModelViewSet):
    serializer_class = NewsletterListSerializer
    queryset = Newsletter.objects.all()

    def get_serializer_class(self):
        if self.action == 'create':
            return NewsletterPostSerializer
        elif self.action == 'retrieve':
            return NewsletterRetrieveSerializer
        return self.serializer_class
    
    @extend_schema(request=NewsletterPostSerializer)
    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)

        newsletter = serializer.instance
        newsletter.send()

        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=201, headers=headers)

    @extend_schema(responses={200: newsletter_messages_response_200})
    @action(detail=True, methods=['GET'])
    def messages(self, request, pk=None):
        newsletter = self.get_object()

        messages = {}
        for status, message_group in newsletter.get_grouped_messages():
            messages[status] = MessageSerializer(message_group).data

        return Response(messages)

    @extend_schema(responses={200: newsletter_message_stats_response_200})
    @action(detail=False, methods=['GET'])
    def message_stats(self, request):
        stats = (
            Newsletter.objects
            .annotate(
                total_messages=Count('message'),
                sent_messages=Count('message', filter=Q(
                    message__status=MessageStatus.SENT)),
                pending_messages=Count('message', filter=Q(
                    message__status=MessageStatus.PENDING)),
                failed_messages=Count('message', filter=Q(
                    message__status=MessageStatus.FAILED)),
            )
            .values(
                'id',
                'start_date',
                'end_date',
                'tag_filter',
                'operator_code_filter',
                'total_messages',
                'sent_messages',
                'pending_messages',
                'failed_messages'
            )
        )

        stats_list = list(stats)

        return Response(stats_list)
