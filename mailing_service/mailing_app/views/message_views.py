from rest_framework import viewsets

from ..models import Message
from ..serializers import MessageSerializer


class MessageViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = MessageSerializer
    queryset = Message.objects.all()
