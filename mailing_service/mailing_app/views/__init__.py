from .client_views import ClientViewSet
from .message_views import MessageViewSet
from .newsletter_views import NewsletterViewSet