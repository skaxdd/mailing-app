from django.db import models


class MessageStatus(models.TextChoices):
    PENDING = 'Pending', 'Pending'
    FAILED = 'Failed', 'Failed'
    SENT = 'Sent', 'Sent'
