import pytest
from datetime import datetime, timedelta
from rest_framework.test import APIClient
from unittest.mock import patch

from ...models import Newsletter

@patch("mailing_app.tasks.send_newsletter.apply_async")
class TestNewsletterViews:

    @pytest.mark.django_db
    def test_create_newsletter_should_return_value_201(self, *args):
        newsletter_values = {
            'start_date': datetime.now(),
            'message_text': 'test_text',
            'operator_code_filter': '123',
            'tag_filter': 'tag_filter',
            'end_date': datetime.now() + timedelta(seconds=10)
        }

        endpoint = '/newsletters/'
        client = APIClient()
        request = client.post(
            endpoint,
            data=newsletter_values,
            format='json'
        )

        assert request.status_code == 201

        newsletter = request.data

        assert newsletter['start_date'] == newsletter_values['start_date'].strftime(
            '%Y-%m-%dT%H:%M:%S.%f')
        assert newsletter['end_date'] == newsletter_values['end_date'].strftime(
            '%Y-%m-%dT%H:%M:%S.%f')
        assert newsletter['message_text'] == newsletter_values['message_text']
        assert newsletter['tag_filter'] == newsletter_values['tag_filter']
        assert newsletter['operator_code_filter'] == newsletter_values['operator_code_filter']

    @pytest.mark.django_db
    def test_list_newsletter_should_return_value_200(self, *args):
        created_newsletter = Newsletter.objects.create(
            start_date=datetime.now(),
            message_text='test_text',
            operator_code_filter='123',
            tag_filter='tag_filter',
            end_date=datetime.now() + timedelta(seconds=10)
        )

        endpoint = '/newsletters/'
        client = APIClient()
        request = client.get(endpoint)

        assert request.status_code == 200
        assert len(request.data) == Newsletter.objects.count()

        newsletter = request.data[0]

        assert newsletter['start_date'] == created_newsletter.start_date.strftime(
            '%Y-%m-%dT%H:%M:%S.%f')
        assert newsletter['end_date'] == created_newsletter.end_date.strftime(
            '%Y-%m-%dT%H:%M:%S.%f')
        assert newsletter['message_text'] == created_newsletter.message_text
        assert newsletter['tag_filter'] == created_newsletter.tag_filter
        assert newsletter['operator_code_filter'] == created_newsletter.operator_code_filter

    @pytest.mark.django_db
    def test_retrieve_newsletter_should_return_value_200(self, *args):
        created_newsletter = Newsletter.objects.create(
            start_date=datetime.now(),
            message_text='test_text',
            operator_code_filter='123',
            tag_filter='tag_filter',
            end_date=datetime.now() + timedelta(seconds=10)
        )

        endpoint = f'/newsletters/{created_newsletter.id}/'
        client = APIClient()
        request = client.get(endpoint)

        assert request.status_code == 200

        newsletter = request.data

        assert newsletter['start_date'] == created_newsletter.start_date.strftime(
            '%Y-%m-%dT%H:%M:%S.%f')
        assert newsletter['end_date'] == created_newsletter.end_date.strftime(
            '%Y-%m-%dT%H:%M:%S.%f')
        assert newsletter['message_text'] == created_newsletter.message_text
        assert newsletter['tag_filter'] == created_newsletter.tag_filter
        assert newsletter['operator_code_filter'] == created_newsletter.operator_code_filter

    @pytest.mark.django_db
    def test_update_newsletter_should_change_values_200(self, *args):
        created_newsletter = Newsletter.objects.create(
            start_date=datetime.now(),
            message_text='test_text',
            operator_code_filter='123',
            tag_filter='tag_filter',
            end_date=datetime.now() + timedelta(seconds=10)
        )

        new_newsletter_values = {
            'start_date': datetime.now() + timedelta(days=1),
            'message_text': 'text_test',
            'operator_code_filter': '321',
            'tag_filter': 'filter_tag',
            'end_date': datetime.now() + timedelta(days=1) + timedelta(seconds=1)
        }

        endpoint = f'/newsletters/{created_newsletter.id}/'
        client = APIClient()
        request = client.patch(
            endpoint,
            data=new_newsletter_values,
            format='json'
        )

        assert request.status_code == 200

        newsletter = request.data

        assert newsletter['start_date'] == new_newsletter_values['start_date'].strftime(
            '%Y-%m-%dT%H:%M:%S.%f')
        assert newsletter['end_date'] == new_newsletter_values['end_date'].strftime(
            '%Y-%m-%dT%H:%M:%S.%f')
        assert newsletter['message_text'] == new_newsletter_values['message_text']
        assert newsletter['tag_filter'] == new_newsletter_values['tag_filter']
        assert newsletter['operator_code_filter'] == new_newsletter_values['operator_code_filter']

    @pytest.mark.django_db
    def test_delete_newsletter_should_delete_204(self, *args):
        created_newsletter = Newsletter.objects.create(
            start_date=datetime.now(),
            message_text='test_text',
            operator_code_filter='123',
            tag_filter='tag_filter',
            end_date=datetime.now() + timedelta(seconds=1)
        )

        endpoint = f'/newsletters/{created_newsletter.id}/'
        client = APIClient()
        request = client.delete(endpoint)

        assert request.status_code == 204
        assert Newsletter.objects.count() == 0
