import pytest
from datetime import datetime, timedelta
from rest_framework.test import APIClient

from ...models import Message, Client, Newsletter


class TestMessageViews:

    @pytest.mark.django_db
    def test_message_list_should_return_200(self):
        endpoint = '/messages/'
        client = APIClient()
        request = client.get(endpoint)

        assert request.status_code == 200

    @pytest.mark.django_db
    def test_message_list_should_return_value(self):
        test_client = Client.objects.create(
            phone_number='71234512345',
            operator_code='123',
            tag='test_tag',
            timezone='UTC'
        )

        start_time = datetime.now()

        test_newsletter = Newsletter.objects.create(
            start_date=start_time,
            message_text='test_text',
            operator_code_filter='123',
            tag_filter='tag_filter',
            end_date=start_time + timedelta(days=3)
        )

        created_message = Message.objects.create(
            sent_at=start_time + timedelta(days=3, minutes=1),
            newsletter=test_newsletter,
            client=test_client,
        )

        endpoint = '/messages/'
        client = APIClient()
        request = client.get(endpoint)

        assert request.status_code == 200
        assert len(request.data) == Message.objects.count()

        message = request.data[0]

        assert message['sent_at'] == created_message.sent_at.strftime(
            '%Y-%m-%dT%H:%M:%S.%f')
        assert message['newsletter'] == created_message.newsletter.id
        assert message['client'] == created_message.client.id
        assert message['status'] == 'Pending'

    @pytest.mark.django_db
    def test_message_retrieve_should_return_value(self):
        test_client = Client.objects.create(
            phone_number='71234512345',
            operator_code='123',
            tag='test_tag',
            timezone='UTC'
        )

        start_time = datetime.now()

        test_newsletter = Newsletter.objects.create(
            start_date=start_time,
            message_text='test_text',
            operator_code_filter='123',
            tag_filter='tag_filter',
            end_date=start_time + timedelta(days=3)
        )

        created_message = Message.objects.create(
            sent_at=start_time + timedelta(days=3, minutes=1),
            newsletter=test_newsletter,
            client=test_client,
        )

        endpoint = f'/messages/{created_message.id}/'
        client = APIClient()
        request = client.get(endpoint)

        assert request.status_code == 200

        message = request.data

        assert message['sent_at'] == created_message.sent_at.strftime(
            '%Y-%m-%dT%H:%M:%S.%f')
        assert message['newsletter'] == created_message.newsletter.id
        assert message['client'] == created_message.client.id
        assert message['status'] == 'Pending'
