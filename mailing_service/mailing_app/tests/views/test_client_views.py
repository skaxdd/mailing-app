import pytest
from rest_framework.test import APIClient

from ...models import Client


class TestClientViews:

    @pytest.mark.django_db
    def test_create_client_should_return_value_201(self):
        client_values = {
            'phone_number': '70000000000',
            'operator_code': '123',
            'tag': 'test_tag',
            'timezone': 'UTC'
        }

        endpoint = '/clients/'
        client = APIClient()
        request = client.post(
            endpoint,
            data=client_values,
            format='json'
        )

        assert request.status_code == 201

        client = request.data

        assert client['phone_number'] == client_values['phone_number']
        assert client['operator_code'] == client_values['operator_code']
        assert client['tag'] == client_values['tag']
        assert client['timezone'] == client_values['timezone']

    @pytest.mark.django_db
    def test_list_client_should_return_value_200(self):
        created_client = Client.objects.create(
            phone_number='70000000000',
            operator_code='123',
            tag='test_tag',
            timezone='UTC'
        )

        endpoint = '/clients/'
        client = APIClient()
        request = client.get(endpoint)

        assert request.status_code == 200
        assert len(request.data) == Client.objects.count()

        client = request.data[0]

        assert client['phone_number'] == created_client.phone_number
        assert client['operator_code'] == created_client.operator_code
        assert client['tag'] == created_client.tag
        assert client['timezone'] == created_client.timezone

    @pytest.mark.django_db
    def test_retrieve_client_should_return_value_200(self):
        created_client = Client.objects.create(
            phone_number='70000000000',
            operator_code='123',
            tag='test_tag',
            timezone='UTC'
        )

        endpoint = f'/clients/{created_client.id}/'
        client = APIClient()
        request = client.get(endpoint)

        assert request.status_code == 200

        client = request.data

        assert client['phone_number'] == created_client.phone_number
        assert client['operator_code'] == created_client.operator_code
        assert client['tag'] == created_client.tag
        assert client['timezone'] == created_client.timezone

    @pytest.mark.django_db
    def test_update_client_should_change_values_200(self):
        created_client = Client.objects.create(
            phone_number='70000000000',
            operator_code='123',
            tag='test_tag',
            timezone='UTC'
        )

        new_client_values = {
            'phone_number': '70000000001',
            'operator_code': '321',
            'tag': 'tag_test',
            'timezone': 'Europe/Moscow'
        }

        endpoint = f'/clients/{created_client.id}/'
        client = APIClient()
        request = client.patch(
            endpoint,
            data=new_client_values,
            format='json'
        )

        assert request.status_code == 200

        client = request.data

        assert client['phone_number'] == new_client_values['phone_number']
        assert client['operator_code'] == new_client_values['operator_code']
        assert client['tag'] == new_client_values['tag']
        assert client['timezone'] == new_client_values['timezone']

    @pytest.mark.django_db
    def test_delete_client_should_delete_204(self):
        created_client = Client.objects.create(
            phone_number='70000000000',
            operator_code='123',
            tag='test_tag',
            timezone='UTC'
        )

        endpoint = f'/clients/{created_client.id}/'
        client = APIClient()
        request = client.delete(endpoint)

        assert request.status_code == 204
        assert Client.objects.count() == 0
