import pytest
from datetime import datetime, timedelta

from ...models import Client, Message, Newsletter


class TestMessageModel():

    @pytest.mark.django_db
    def test_create_message(self):
        test_client = Client.objects.create(
            phone_number='71234512345',
            operator_code='123',
            tag='test_tag',
            timezone='UTC'
        )

        start_time = datetime.now()

        test_newsletter = Newsletter.objects.create(
            start_date=start_time,
            message_text='test_text',
            operator_code_filter='123',
            tag_filter='tag_filter',
            end_date=start_time + timedelta(days=3)
        )

        test_message = Message.objects.create(
            sent_at=start_time + timedelta(days=3, minutes=1),
            newsletter=test_newsletter,
            client=test_client,
            status='Pending'
        )

        assert Message.objects.count() == 1

        assert test_message.sent_at == start_time + \
            timedelta(days=3, minutes=1)
        assert test_message.newsletter == test_newsletter
        assert test_message.status == 'Pending'
        assert test_message.client == test_client
