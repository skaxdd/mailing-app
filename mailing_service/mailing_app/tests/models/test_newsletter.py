import pytest
from datetime import datetime, timedelta

from ...models import Newsletter


class TestNewsletterModel():

    @pytest.mark.django_db
    def test_create_newsletter(self):
        start_time = datetime.now()

        test_newsletter = Newsletter.objects.create(
            start_date=start_time,
            message_text='test_text',
            operator_code_filter='123',
            tag_filter='test_tag_filter',
            end_date=start_time + timedelta(days=3)
        )

        assert Newsletter.objects.count() == 1

        assert test_newsletter.start_date == start_time
        assert test_newsletter.message_text == 'test_text'
        assert test_newsletter.operator_code_filter == '123'
        assert test_newsletter.tag_filter == 'test_tag_filter'
        assert test_newsletter.end_date == start_time + timedelta(days=3)
