import pytest

from ...models import Client


class TestClientModel():

    @pytest.mark.django_db
    def test_create_client(self):
        test_client = Client.objects.create(
            phone_number='71234512345',
            operator_code='123',
            tag='test_tag',
            timezone='UTC'
        )

        assert Client.objects.count() == 1

        assert test_client.phone_number == '71234512345'
        assert test_client.operator_code == '123'
        assert test_client.tag == 'test_tag'
        assert test_client.timezone == 'UTC'
