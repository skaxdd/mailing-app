from django.db import models

from .client import Client
from .newsletter import Newsletter
from ..enums import MessageStatus


class Message(models.Model):
    sent_at = models.DateTimeField(blank=True, null=True)
    status = models.CharField(
        max_length=10,
        choices=MessageStatus.choices,
        default=MessageStatus.PENDING
    )
    newsletter = models.ForeignKey(
        Newsletter, 
        on_delete=models.CASCADE,
        related_name="messages"
    )
    client = models.ForeignKey(Client, on_delete=models.CASCADE)

    def __str__(self):
        return f'Message to {self.client} - {self.status}'
