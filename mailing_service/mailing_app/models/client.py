from django.db import models

from ..managers import ClientManager


class Client(models.Model):
    phone_number = models.CharField(
        max_length=11,
        unique=True,
    )
    operator_code = models.CharField(max_length=3)
    tag = models.CharField(max_length=255)
    timezone = models.CharField(max_length=64)

    objects = ClientManager()

    def __str__(self):
        return self.phone_number
