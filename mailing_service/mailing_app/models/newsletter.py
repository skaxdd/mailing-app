from django.db import models
from django.utils import timezone
from itertools import groupby

from .client import Client
from ..tasks import send_newsletter

class Newsletter(models.Model):
    start_date = models.DateTimeField()
    message_text = models.TextField()
    operator_code_filter = models.CharField(
        max_length=3,
        null=True,
        blank=True)
    tag_filter = models.CharField(
        max_length=255,
        null=True,
        blank=True
    )
    end_date = models.DateTimeField()

    def __str__(self):
        return self.message_text

    def send(self):
        target_clients = []

        if self.tag_filter:
            target_clients = Client.objects.filter_by_tag(
                self.tag_filter
            ).values_list("id", flat=True)
        
        if self.operator_code_filter:
            target_clients = Client.objects.filter_by_operator_code(
                self.operator_code_filter
            ).values_list("id", flat=True)

        if self.start_date > timezone.now() or self.start_date < timezone.now() < self.end_date:
            send_newsletter.apply_async(
                args=(self.id, list(target_clients)), 
                eta=self.start_date,
                expires=self.end_date)
            
    def get_messages(self):
        return self.messages.all().order_by("status")
    
    def get_grouped_messages(self):
        messages = self.get_messages()

        message_groups = {}
        for status, group in groupby(messages, key=lambda message: message.status):
            message_groups[status] = list(group)

        return message_groups
