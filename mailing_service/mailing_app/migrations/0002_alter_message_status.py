# Generated by Django 4.2.5 on 2023-10-12 17:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mailing_app', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='message',
            name='status',
            field=models.CharField(choices=[('Pending', 'Pending'), ('Failed', 'Failed'), ('Sent', 'Sent')], default='Pending', max_length=10),
        ),
    ]
