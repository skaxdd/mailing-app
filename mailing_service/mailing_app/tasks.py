from django.apps import apps
from django.utils import timezone
from celery import shared_task
from celery.exceptions import MaxRetriesExceededError

from .constants import (
    MAILING_MAX_RETRIES,
    MAILING_RETRIES_SEC_COUNTDOWN,
)
from .utils import send_message
from .enums import MessageStatus


@shared_task(bind=True, max_retries=MAILING_MAX_RETRIES)
def send_newsletter(self, newsletter_id, client_ids):

    # importing model by string to avoid circular import 
    newsletter_model = apps.get_model("mailing_app", "newsletter")
    client_model = apps.get_model("mailing_app", "client")
    message_model = apps.get_model("mailing_app", "message")

    newsletter = newsletter_model.objects.get(id=newsletter_id)
    clients = client_model.objects.filter(id__in=client_ids)

    for client in clients:
        message, created = message_model.objects.get_or_create(
            newsletter=newsletter,
            client=client,
        )

        if created or message.status != MessageStatus.SENT:

            is_msg_completed = send_message(
                message.id, client.phone_number, newsletter.message_text
            )

            if is_msg_completed:
                message.status = MessageStatus.SENT
                message.sent_at = timezone.now()
            else:
                message.status = MessageStatus.FAILED
                try:
                    raise self.retry(countdown=MAILING_RETRIES_SEC_COUNTDOWN)
                except MaxRetriesExceededError:
                    pass

        message.save()
