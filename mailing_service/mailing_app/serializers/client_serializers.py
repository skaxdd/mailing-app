import re
from rest_framework import serializers

from ..constants import PHONE_REGEX
from ..models import Client


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = '__all__'

    def validate_phone_number(self, value):
        if not re.match(PHONE_REGEX, value):
            raise serializers.ValidationError(
                'Phone number format is 7xxxxxxxxxx'
            )
        return value
