from rest_framework import serializers

from .message_serializers import MessageSerializer
from ..models import Newsletter


class NewsletterListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Newsletter
        fields = '__all__'

class NewsletterRetrieveSerializer(serializers.ModelSerializer):
    messages = MessageSerializer(many=True, read_only=True)

    class Meta:
        model = Newsletter
        fields = (
            'id',
            'start_date',
            'end_date',
            'message_text',
            'operator_code_filter',
            'tag_filter',
            'messages'
        )


class NewsletterPostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Newsletter
        fields = '__all__'

    def validate(self, data):
        if data['start_date'] > data['end_date']:
            raise serializers.ValidationError(
                'Start date should be earilier than end date'
            )

        return data
