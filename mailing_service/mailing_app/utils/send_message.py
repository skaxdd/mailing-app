import json
import requests
from requests.exceptions import ConnectionError

from ..constants import MAILING_SERVICE_URL
from mailing_service.settings import API_TOKEN

def send_message(msg_id, client_phone, msg_text):
    url = f'{MAILING_SERVICE_URL}/{msg_id}'
    headers = {'Authorization': API_TOKEN}

    request_body = json.dumps({
        'id': int(msg_id),
        'phone': int(client_phone),
        'text': msg_text
    })

    try:
        response = requests.post(
            url, 
            data=request_body, 
            headers=headers
        )
    except ConnectionError:
        return False

    if response.status_code == 200:
        return True
    return False