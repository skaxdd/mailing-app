_message = {
    "type": "object",
    "properties": {
        "id": {"type": "integer"},
        "sent_at": {"type": "string", "format": "date-time"},
        "status": {"type": "string"},
        "newsletter": {"type": "integer"},
        "client": {"type": "integer"},
    },
}

newsletter_messages_response_200 = {
    "type": "object",
    "properties": {
        "Sent": {
            "type": "array",
            "items": _message
        },
        "Pending": {
            "type": "array",
            "items": _message
        },
        "Failed": {
            "type": "array",
            "items": _message
        },
    },
}
