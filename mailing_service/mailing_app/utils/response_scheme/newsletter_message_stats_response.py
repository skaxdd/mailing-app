newsletter_message_stats_response_200 = {
    "type": "array",
    "items": {
        "type": "object",
        "properties": {
            "id": {"type": "integer"},
            "start_date": {"type": "string", "format": "date-time"},
            "end_date": {"type": "string", "format": "date-time"},
            "tag_filter": {"type": "string"},
            "operator_code_filter": {"type": "string"},
            "total_messages": {"type": "integer"},
            "sent_messages": {"type": "integer"},
            "failed_messages": {"type": "integer"},
        },
    },
}
