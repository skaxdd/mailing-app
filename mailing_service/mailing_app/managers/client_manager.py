from django.db import models


class ClientManager(models.Manager):
    def filter_by_tag(self, tag):
        if not tag:
            return self.all()
        return self.filter(tag__in=tag.split(","))

    def filter_by_operator_code(self, operator_code):
        if not operator_code:
            return self.all()
        return self.filter(operator_code__in=operator_code.split(","))