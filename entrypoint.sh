#! /bin/bash

cd mailing_service

python -m pytest

if [ $? -ne 0 ]; then
    echo "Test session has failed."
    exit 1
fi


python -m celery -A mailing_service worker -l info --detach
python manage.py migrate
export DJANGO_SUPERUSER_PASSWORD=pass
export DJANGO_SUPERUSER_USERNAME=admin
export DJANGO_SUPERUSER_EMAIL=admin@example.com
python manage.py createsuperuser --noinput

gunicorn \
--access-logfile /var/log/gunicorn.access.log \
--error-logfile /var/log/gunicorn.error.log \
--capture-output \
--enable-stdio-inheritance \
mailing_service.wsgi:application --bind "0.0.0.0:8000" --reload